#include<stdio.h>
#include<math.h>
void input(float *a , float *b , float *c)
{
    printf("Enter the co-efficients a b and c\n") ;
        scanf("%f%f%f",a,b,c) ;
}
void compute (float a , float b , float c , float *discriminant , float *root1 , float *root2)
{
    *discriminant = (b * b) - (4 * a * c);
    if (discriminant > 0)
    {
        *root1 = (-b + sqrt(*discriminant)) / (2 * a);
        *root2 = (-b - sqrt(*discriminant)) / (2 * a);
    }
    else if (discriminant == 0)          
    {
        *root1 = *root2 = -b / (2 * a);
    }
    else
        *root1 = b/(2*a) + (sqrt(-*discriminant)/(2*a));
    *root2 = b/(2*a) + (sqrt(-*discriminant)/(2*a)) ;
}
void display(float a , float b , float c , float discriminant , float *root1 , float *root2) 
{
    if (discriminant > 0)
    {
        printf("Roots are real \n root1 = %f and root2 = %f", *root1, *root2);
    }
    else if (discriminant == 0)     
    {
        printf("Roots are equal\n");
        printf("root1 = root2 = %f;", *root1);
    }       
    else 
    {
        printf("Roots are imaginary\n");
        printf("First root = %f + i%f\n", -b/(2*a), sqrt(-discriminant)/(2*a));
        printf("Second root = %f - i%f\n", -b/(2*a), sqrt(-discriminant)/(2*a));
    } 
}
int main()
{
    float x,y,z,d,r1,r2 ;
    input(&x,&y,&z) ;
    compute(x,y,z,&d,&r1,&r2) ;
    display(x,y,x,d,&r1,&r2) ;
    return 0 ;
}
