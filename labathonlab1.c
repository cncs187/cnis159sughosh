#include<stdio.h>
void input(float *principal , float *time , float *interest)
{
	printf("Enter the principal amount\n");
	scanf("%f",principal) ;
	printf("Enter the Time period in Years and Months\n");
	scanf("%f",time);
	printf("Enter the rate of interest\n");
	scanf("%f",interest);
}
void compute(float principal , float time , float interest , float *si)
{
	*si = (principal*time*interest)/100 ;
}
void display (float principal , float time , float interest , float si)
{
	printf(" Principal Amount is %f \n Time period is %f \n Rate of Interest is %f \n The simple Interest is %f \n",principal,time,interest,si) ;
}
int main()
{
	float p,t,r,simple_interest ;
	input(&p,&t,&r);
	compute(p,t,r,&simple_interest) ;
	display(p,t,r,simple_interest);
	return 0 ;
}
