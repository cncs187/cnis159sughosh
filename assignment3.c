#include <math.h>
#include <stdio.h>
int main()
{
    float a, b, c, discriminant, root1, root2 ;;
    printf("Enter coefficients a, b and c: ");
    scanf("%f %f %f", &a, &b, &c);

    discriminant = b * b - 4 * a * c;

    if (discriminant > 0)
    {
        root1 = (-b + sqrt(discriminant)) / (2 * a);
        root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real \n root1 = %f and root2 = %f", root1, root2);
    }

    else if (discriminant == 0)          
    {
        printf("Roots are equal\n");
        root1 = root2 = -b / (2 * a);
        printf("root1 = root2 = %f;", root1);
    }

    else 
    {
        printf("Roots are imaginary\n");
        printf("First root = %f + i%f\n", -b/(2*a), sqrt(-discriminant)/(2*a));
        printf("Second root = %f - i%f\n", -b/(2*a), sqrt(-discriminant)/(2*a));
    } 
 
    return 0;
}