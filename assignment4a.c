#include<stdio.h>
void swap_value(int x ,int y)
{
    int temp ;
    temp =x ;
    x= y ;
    y = temp ;
    printf("The value of x and y after swapping inside the function is %d %d\n",x,y);
}
void swap_pass_reference(int *x , int *y)
{
    int temp ;
    temp = *x ;
    *x = *y ;
    *y = temp ;
    printf("The value of x and y after swapping inside the fucntions %d %d\n",*x,*y);
}
int main()
{
    int p,q ;
    printf("Enter two numbers\n");
    scanf("%d%d",&p,&q);
    swap_value(p,q);
    printf("The value of p and q after calling swap in main function %d,%d",p,q);
    swap_pass_reference(&p,&q);
    printf("The value of p and q after calling swap in main function %d,%d",p,q);
    return 0;
}